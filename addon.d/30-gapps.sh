#!/sbin/sh
#
# ADDOND_VERSION=2
#
# /system/addon.d/30-gapps.sh
#
. /tmp/backuptool.functions

list_files() {
cat <<EOF
priv-app/Velvet/Velvet.apk
priv-app/SetupWizard/SetupWizard.apk
priv-app/PrebuiltGmsCore/PrebuiltGmsCore.apk
priv-app/Phonesky/Phonesky.apk
priv-app/GoogleServicesFramework/GoogleServicesFramework.apk
priv-app/GooglePartnerSetup/GooglePartnerSetup.apk
priv-app/GoogleFeedback/GoogleFeedback.apk
priv-app/GoogleBackupTransport/GoogleBackupTransport.apk
priv-app/AndroidMigratePrebuilt/AndroidMigratePrebuilt.apk
lib64/libjni_latinimegoogle.so
lib/libjni_latinimegoogle.so
framework/com.google.android.maps.jar
framework/com.google.android.dialer.support.jar
etc/sysconfig/google_build.xml
etc/sysconfig/google.xml
etc/sysconfig/google-hiddenapi-package-whitelist.xml
etc/permissions/privapp-permissions-google.xml
etc/permissions/com.google.android.maps.xml
etc/permissions/com.google.android.dialer.support.xml
etc/default-permissions/default-permissions.xml
app/PrebuiltExchange3Google/PrebuiltExchange3Google.apk
app/MarkupGoogle/lib/arm64/libsketchology_native.so
app/MarkupGoogle/MarkupGoogle.apk
app/GoogleContactsSyncAdapter/GoogleContactsSyncAdapter.apk
app/GoogleCalendarSyncAdapter/GoogleCalendarSyncAdapter.apk
addon.d/30-gapps.sh
addon.d/addond_tail
addon.d/addond_head
EOF
}

case "$1" in
  backup)
    list_files | while read FILE DUMMY; do
      backup_file $S/$FILE
    done
  ;;
  restore)
    list_files | while read FILE REPLACEMENT; do
      R=""
      [ -n "$REPLACEMENT" ] && R="$S/$REPLACEMENT"
      [ -f "$C/$S/$FILE" ] && restore_file $S/$FILE $R
    done
  ;;
  pre-backup)
    # Stub
  ;;
  post-backup)
    # Stub
  ;;
  pre-restore)
    # Stub
  ;;
  post-restore)
    if [ -d "/postinstall" ]; then
      P="/postinstall/system"
    else
      P="/system"
    fi

    for i in $(list_files); do
      chown root:root "$P/$i"
      chmod 644 "$P/$i"
      chmod 755 "$(dirname "$P/$i")"
    done
  ;;
esac
