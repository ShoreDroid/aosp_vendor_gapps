#
# PACROM Gapps Inclusion Makefile
#

GAPPS_PATH := vendor/gapps

# addon.d
PRODUCT_COPY_FILES += \
    $(GAPPS_PATH)/addon.d/addond_head:system/addon.d/addond_head \
    $(GAPPS_PATH)/addon.d/addond_tail:system/addon.d/addond_tail \
    $(GAPPS_PATH)/addon.d/30-gapps.sh:system/addon.d/30-gapps.sh \
    $(GAPPS_PATH)/addon.d/50-lineage.sh:system/addon.d/50-lineage.sh

# app    
PRODUCT_PACKAGES += \
    GoogleCalendarSyncAdapter \
    GoogleContactsSyncAdapter \
    MarkupGoogle \
    PrebuiltExchange3Google 

# app
PRODUCT_COPY_FILES += \
    $(GAPPS_PATH)/app/MarkupGoogle/lib/arm64/libsketchology_native.so:system/app/MarkupGoogle/lib/arm64/libsketchology_native.so

# etc
PRODUCT_COPY_FILES += \
    $(GAPPS_PATH)/etc/default-permissions/default-permissions.xml:system/etc/default-permissions/default-permissions.xml \
    $(GAPPS_PATH)/etc/permissions/com.google.android.dialer.support.xml:system/etc/permissions/com.google.android.dialer.support.xml \
    $(GAPPS_PATH)/etc/permissions/com.google.android.maps.xml:system/etc/permissions/com.google.android.maps.xml \
    $(GAPPS_PATH)/etc/permissions/privapp-permissions-google.xml:system/etc/permissions/privapp-permissions-google.xml \
    $(GAPPS_PATH)/etc/sysconfig/google-hiddenapi-package-whitelist.xml:system/etc/sysconfig/google-hiddenapi-package-whitelist.xml \
    $(GAPPS_PATH)/etc/sysconfig/google.xml:system/etc/sysconfig/google.xml \
    $(GAPPS_PATH)/etc/sysconfig/google_build.xml:system/etc/sysconfig/google_build.xml
    
# framework
PRODUCT_COPY_FILES += \
    $(GAPPS_PATH)/framework/com.google.android.dialer.support.jar:system/framework/com.google.android.dialer.support.jar \
    $(GAPPS_PATH)/framework/com.google.android.maps.jar:system/framework/com.google.android.maps.jar

# lib
PRODUCT_COPY_FILES += \
    $(GAPPS_PATH)/lib/libjni_latinimegoogle.so:system/lib/libjni_latinimegoogle.so
    
    # lib64
PRODUCT_COPY_FILES += \
    $(GAPPS_PATH)/lib64/libjni_latinimegoogle.so:system/lib64/libjni_latinimegoogle.so

# priv-apps    
PRODUCT_PACKAGES += \
    AndroidMigratePrebuilt \
    GoogleBackupTransport \
    GoogleFeedback \
    GooglePartnerSetup \
    GoogleServicesFramework \
    Phonesky \
    PrebuiltGmsCore\
    SetupWizard \
    Velvet
    